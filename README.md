# Web de Catálogos

Web de Triathlon Catálogos: [http://catalogothn.com.pe](http://catalogothn.com.pe) 

## Recursos

- **Hosting:** [Digital Ocean](https://www.digitalocean.com/)
- **Dominio:** [Punto.pe](https://punto.pe/)
- **Visor de catálogos:** [Flipping Book](https://flippingbook.com/)

## Stack de Tecnología

- WordPress (PHP & MySQL)
  * Ninja Forms
  * Easy WP SMTP
- jQuery
- SweetAlert2

## Objetivo

Este sitio web administrable, cuenta con 4 páginas: **Home**, **Catálogo**, **Afíliate** y **Contáctanos**. 
El contenido de éstas páginas es configurable a través del [administrador](http://catalogothn.com.pe/admin).

A continuación se detalla a grandes rasgos el contenido de cada página:

### Home

En esta página se muestra un slider configurable de últimas novedades, además de los beneficios que ofrece formar parte
de Triathlon Catálogos, las marcas con las que se trabaja y un footer con enlaces de navegación útiles.

### Catálogo

Para acceder a esta página es necesaria hacer una validación de compra del catálogo en tienda, esto es posible lograr ingresando
el número de documento impreso en el ticket y éste realiza la validación a través
del [API](https://gitlab.com/triathlon-catalogos/api).
Una vez validado, el usuario podrá ver los últimos 10 catálogos subidos a _Flipping Book_, y este registro es configurable mediante
el [administrador del sitio](http://catalogothn.com.pe/admin).

### Afíliate

Este es un formulario que solicita información a los usuarios para que alguien de Triathlon Catálogos se pueda contactar y 
explicarles el proceso de afiliación como asesor(a). Una vez ingresado los datos, este envía un correo al usuario y también
al correo `afiliate@triathlon.com.pe`.

### Contáctanos

Esta página muestra información sobre las tiendas y teléfonos de contacto de Catálogos.

> ## Nota
>
> Este documento solo indica las funcionalidades programadas 
> como requerimiento de Triathlon Catálogos, si algo nuevo fue
> programado por algún colaborador de la empresa en mención,
> puede actualizar este documento.